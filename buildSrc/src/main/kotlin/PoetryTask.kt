package nabaal.gradle

import com.fs.starfarer.api.combat.MutableShipStatsAPI
import com.fs.starfarer.api.combat.MutableStat
import com.fs.starfarer.api.combat.StatBonus
import org.gradle.api.*
import org.gradle.api.tasks.*
import com.squareup.kotlinpoet.*
import java.nio.file.Paths
import javax.annotation.Generated

open class PoetryTask : DefaultTask() {
	private var outname = "Poetry"
	private var outdir = ""

	init {
		group = "generation"
		description = "Generates PITA kotlin source code with KotlinPoet."
	}

	/** Set file name. **/
	fun name(value: String) {
		outname = value
	}

	/** Set output directory. **/
	fun dir(value: String) {
		outdir = value
	}

	@TaskAction
	fun run() {
		FileSpec.builder("", outname)
			.addFunction(genMutableShipStatsAPI())
			.build().writeTo(Paths.get(outdir))
	}

	private fun genMutableShipStatsAPI(): FunSpec {
		val function = FunSpec.builder("applyId")
			.receiver(MutableShipStatsAPI::class)
			.addParameter("other", MutableShipStatsAPI::class)
			.addParameter("id", String::class)
			.addAnnotation(genPoetAnnotation())

		val methods = MutableShipStatsAPI::class.java.methods
		methods.sortBy { it.name }
		methods.forEach {
			if (!it.name.startsWith("get") || it.getAnnotation(java.lang.Deprecated::class.java) != null) return@forEach
			if (it.returnType == MutableStat::class.java || it.returnType == StatBonus::class.java) {
				val name = StringBuilder(it.name.drop(3))
				for ((i, c) in name.withIndex()) {

					val n = i + 1
					if (i != 0 && (name.length <= n || name[n].isLowerCase()))
						break


					name.setCharAt(i, c.toLowerCase())
				}
				function.addStatement("$name.applyId(other.$name, id)")
			}
		}

		return function.build()
	}

	private fun genPoetAnnotation(): AnnotationSpec {
		val annotation = AnnotationSpec.builder(Generated::class)
			.addMember("%S", "com.squareup.kotlinpoet")

		return annotation.build()
	}
}
