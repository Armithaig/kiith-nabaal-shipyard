plugins {
	`kotlin-dsl`
}

repositories { jcenter() }

val starsector: String by project
dependencies {
	implementation("com.squareup:kotlinpoet:1.4.0")
	implementation(fileTree(starsector) { include("*.jar") })
}