import nabaal.gradle.PoetryTask
import org.jetbrains.kotlin.gradle.tasks.KotlinCompile
import proguard.gradle.ProGuardTask

group = "nabaal"
version = "1.3.1"

// Add your starsector path in local gradle.properties
// Ex:	starsector=/home/username/games/starsector/
val starsector: String by project
val jdk: String = System.getProperty("java.home")

plugins {
	kotlin("jvm") version "1.3.50"
	kotlin("plugin.allopen") version "1.3.50"
}

buildscript {
	repositories { jcenter() }
	dependencies { classpath("net.sf.proguard:proguard-gradle:6.2.0") }
}

repositories { jcenter() }

sourceSets {
	main { java.setSrcDirs(listOf("src/nabaal")) }
	test { java.setSrcDirs(listOf<String>()) }
}

dependencies {
	implementation(kotlin("stdlib-jdk7"))
	compileOnly(fileTree(starsector) { include("*.jar") })
}

tasks {
	withType<KotlinCompile> {
		kotlinOptions { jvmTarget = "1.6" }
	}
	jar {
		exclude("META-INF/*.kotlin_module", "META-INF/maven/")
		configurations.runtimeClasspath { from(map { zipTree(it) }) }
	}
	create<ProGuardTask>("dce") {
		dontobfuscate()
		dontoptimize()
		keep("class nabaal.**")
		keepclassmembers("class ** { *; }")
		injars(jar)
		outjars("mod/bin/nabaal.jar")
		libraryjars(fileTree(jdk) { include("**/*.jar") })
		libraryjars(fileTree(starsector) { include("*.jar") })
	}
	create<Exec>("run") {
		dependsOn("dce")
		setWorkingDir(starsector)
		commandLine("./starsector.sh")
		isIgnoreExitValue = true
	}
	create<PoetryTask>("poetry") {
		name("Poetry")
		dir("src/nabaal/util/")
	}
}
