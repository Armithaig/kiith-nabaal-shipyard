package nabaal.util

import com.fs.starfarer.api.Global
import com.fs.starfarer.api.combat.*
import com.fs.starfarer.combat.entities.Ship
import org.lwjgl.util.vector.Vector2f
import javax.annotation.Generated
import kotlin.math.*
import kotlin.random.Random

// Cache last set of rotation results
private var RotAngle = 0.0f
private var RotRad = 0.0
private var RotCos = 1.0
private var RotSin = 0.0

/** Rotate this [2d vector][Vector2f] around origin.
 * Results cached: single thread repeat calls with same [angle] cheap. */
fun Vector2f.rotate(angle: Float): Vector2f {
	if (angle != RotAngle) {
		RotAngle = angle
		RotRad = angle * PI / 180
		RotCos = cos(RotRad)
		RotSin = sin(RotRad)
	}

	return Vector2f((x * RotCos - y * RotSin).toFloat(), (x * RotSin + y * RotCos).toFloat())
}

/** Randomise this [value][Float] by a factor of -1 to 1 with [sign], 0 to 1 otherwise.*/
fun Float.range(sign: Boolean = true): Float {
	var fac = Random.nextFloat()
	if (sign) fac = fac * 2 - 1
	return this * fac
}

fun Vector2f.range(sign: Boolean = true): Vector2f {
	return Vector2f(x.range(sign), y.range(sign))
}

infix fun Vector2f.sqdist(v: Vector2f): Float {
	return (x - v.x).pow(2) + (y - v.y).pow(2)
}

fun Vector2f.diamond(v: Vector2f): Float {
	val dX = v.x - x
	val dY = v.y - y

	return if (dY >= 0) {
		if (dX >= 0) dY / (dX + dY)
		else 1 - dX / (-dX + dY)
	} else {
		if (dX < 0) 2 - dY / (-dX - dY)
		else 3 + dX / (dX - dY)
	}
}

fun Vector2f.angle(v: Vector2f): Float {
	return Global.getSettings().getAngleInDegreesFast(this, v)
}

data class ArmorCell(val x: Int, val y: Int, val value: Float, val fraction: Float)

/** Get active [armor cell][ArmorCell] closest to [v] in [grid][ArmorGridAPI] of this [ship][ShipAPI].
 * Inaccurate: uses sprite to find relevant armor cell instead of bounds. */
fun ShipAPI.closestArmorCell(v: Vector2f): ArmorCell {
	val degrees = (location.angle(v) + 810 - facing) % 360
	val rad = (degrees * (PI / 180)).toFloat()
	var x = cos(rad)
	var y = sin(rad)

	if (abs(x) > abs(y)) {
		x = (x.sign + 1) / 2
		y = (y + 1) / 2
	} else {
		x = (x + 1) / 2
		y = (y.sign + 1) / 2
	}

	val sprite = spriteAPI
	val armor = armorGrid
	val iX = sprite.width * (1f - sprite.textureWidth) / armor.cellSize
	val iY = sprite.height * (1f - sprite.textureHeight) / armor.cellSize
	val cX = round(iX + (armor.grid.size - 1f - iX * 2f) * x).toInt()
	val cY = round(iY + (armor.grid[cX].size - 1f - iY * 2f) * y).toInt()

	return ArmorCell(cX, cY, armor.getArmorValue(cX, cY), armor.getArmorFraction(cX, cY))
}

/** Get degrees rotation required to face location, range of -180 to 180. */
fun CombatEntityAPI.toFace(v: Vector2f): Float {
	val target = location.angle(v)
	return (target - facing + 540) % 360 - 180
}

/*fun spiral(cX: Int, cY: Int) = sequence {
	var x = cX
	var y = cY
	var x2 = 1
	var y2 = 0
	var l = 1
	var p = 1

	while (true) {
		x += x2
		y += y2

		if (p++ >= l) {
			p = 0

			x2 = -y2.also { y2 = x2 }
			if (y2 == 0) l++
		}
		yield(Pair(x, y))
	}
}*/

/** Make this [weapon][WeaponAPI] fire missile projectile of [weaponId] at [location]. */
fun WeaponAPI.fireMissile(
	weaponId: String,
	location: Vector2f,
	soundId: String = weaponId,
	smokeGen: SmokeGenerator? = null
): MissileAPI {
	val combat = Global.getCombatEngine()
	val missile = combat.spawnProjectile(ship, this, weaponId, location, currAngle, ship.velocity) as MissileAPI

	// Generate missile launch smoke, reversing blowback if missiles are internal (not rendered).
	val dir = Vector2f(0f, -16f).apply { if (missileRenderData == null) y = -y }
	val smoke = smokeGen ?: SmokeGenerator().apply {
		momentum = ship.velocity
		size = Pair(8f, 12f)
		duration = 1.5f
		count = 6
	}
	smoke.cloud(location, Vector2f(5f, 5f))
	smoke.line(location, dir.rotate(currAngle + arcFacing))

	if (soundId.isNotEmpty()) {
		val sound = Global.getSoundPlayer()
		sound.playSound(soundId, 1f, 1f, location, ship.velocity)
	}

	return missile
}

data class ShipTarget(var ship: ShipAPI, var range: Float)

fun CombatEntityAPI.selectShips(
	max: Float = Float.POSITIVE_INFINITY,
	min: Float = Float.NEGATIVE_INFINITY,
	condition: (ShipAPI) -> Boolean
): ShipTarget? {
	return selectShips(1, max, min, condition).firstOrNull()
}

/** Return list of the [amount] closest ships to this [entity][CombatEntityAPI] within [min] and [max] that fulfill [condition]. O(n * [amount]).
 * Prefer a rough/partial bucket sort for large [amount]. */
fun CombatEntityAPI.selectShips(
	amount: Int,
	max: Float = Float.POSITIVE_INFINITY,
	min: Float = Float.NEGATIVE_INFINITY,
	condition: (ShipAPI) -> Boolean
): List<ShipTarget> {
	val combat = Global.getCombatEngine()
	val list = ArrayList<ShipTarget>(amount)
	val low = if (min.isFinite()) min.pow(2) else min
	var high = if (max.isFinite()) max.pow(2) else max

	combat.ships.forEach {
		if (this == it || it.isHulk || it.isShuttlePod || !condition(it)) return@forEach
		var trg = ShipTarget(it, it.location sqdist location)

		if (trg.range > high || trg.range < low) return@forEach

		for (i in 0 until amount) {
			if (list.size <= i) {
				list.add(trg)
				return@forEach
			} else if (list[i].range > trg.range) {
				list[i] = trg.also { trg = list[i] }
			}
		}

		if (list.size == amount)
			high = list.last().range

	}

	list.forEach { it.range = sqrt(it.range) }

	return list
}


/** Get [element][Collection.elementAt] at modulus of [index] / [size][Collection.size]. */
fun <T> Collection<T>.modGet(index: Int): T {
	return this.elementAt(index % this.size)
}

val String.lower: String
	get() = this.toLowerCase()

val String.upper: String
	get() = this.toUpperCase()

enum class SystemState { USABLE, ON, FLUX, COOLDOWN, VENT, OVERLOAD, AMMO, DISABLED }

/** Get what usability state system is in, ordinal at degrees of severity for interval apathy. */
val ShipAPI.systemState: SystemState
	get() {
		if (system.isOn) return SystemState.ON
		if (system.cooldownRemaining > 0 || system.isActive) return SystemState.COOLDOWN
		if (system.fluxTotal + currFlux >= maxFlux) return SystemState.FLUX
		if (system.isOutOfAmmo) return SystemState.AMMO
		if (fluxTracker.isOverloaded) return SystemState.OVERLOAD
		if (fluxTracker.isVenting) return SystemState.VENT
		if (this is Ship && isSystemDisabled) return SystemState.DISABLED
		return SystemState.USABLE
	}

val ShipSystemAPI.fluxTotal: Float
	get() = fluxPerUse + fluxPerSecond * (chargeUpDur + chargeActiveDur)

// region MutableShipStatsAPI extensions
fun MutableStat.applyId(other: MutableStat, id: String) {
	other.flatMods[id]?.let { modifyFlatAlways(it.source, it.value, it.desc) }
	other.percentMods[id]?.let { modifyPercentAlways(it.source, it.value, it.desc) }
	other.multMods[id]?.let { modifyMultAlways(it.source, it.value, it.desc) }
}

fun StatBonus.applyId(other: StatBonus, id: String) {
	other.flatBonuses[id]?.let { modifyFlatAlways(it.source, it.value, it.desc) }
	other.percentBonuses[id]?.let { modifyPercentAlways(it.source, it.value, it.desc) }
	other.multBonuses[id]?.let { modifyMultAlways(it.source, it.value, it.desc) }
}

/** Make [this] inherit all stats from [other] with [id]. */
@Generated("com.squareup.kotlinpoet")
fun MutableShipStatsAPI.applyId(other: MutableShipStatsAPI, id: String) {
	acceleration.applyId(other.acceleration, id)
	armorBonus.applyId(other.armorBonus, id)
	armorDamageTakenMult.applyId(other.armorDamageTakenMult, id)
	autofireAimAccuracy.applyId(other.autofireAimAccuracy, id)
	ballisticAmmoBonus.applyId(other.ballisticAmmoBonus, id)
	ballisticRoFMult.applyId(other.ballisticRoFMult, id)
	ballisticWeaponDamageMult.applyId(other.ballisticWeaponDamageMult, id)
	ballisticWeaponFluxCostMod.applyId(other.ballisticWeaponFluxCostMod, id)
	ballisticWeaponRangeBonus.applyId(other.ballisticWeaponRangeBonus, id)
	baseCRRecoveryRatePercentPerDay.applyId(other.baseCRRecoveryRatePercentPerDay, id)
	beamDamageTakenMult.applyId(other.beamDamageTakenMult, id)
	beamPDWeaponRangeBonus.applyId(other.beamPDWeaponRangeBonus, id)
	beamShieldDamageTakenMult.applyId(other.beamShieldDamageTakenMult, id)
	beamWeaponDamageMult.applyId(other.beamWeaponDamageMult, id)
	beamWeaponFluxCostMult.applyId(other.beamWeaponFluxCostMult, id)
	beamWeaponRangeBonus.applyId(other.beamWeaponRangeBonus, id)
	beamWeaponTurnRateBonus.applyId(other.beamWeaponTurnRateBonus, id)
	breakProb.applyId(other.breakProb, id)
	crLossPerSecondPercent.applyId(other.crLossPerSecondPercent, id)
	crPerDeploymentPercent.applyId(other.crPerDeploymentPercent, id)
	cargoMod.applyId(other.cargoMod, id)
	combatEngineRepairTimeMult.applyId(other.combatEngineRepairTimeMult, id)
	combatWeaponRepairTimeMult.applyId(other.combatWeaponRepairTimeMult, id)
	crewLossMult.applyId(other.crewLossMult, id)
	criticalMalfunctionChance.applyId(other.criticalMalfunctionChance, id)
	criticalMalfunctionDamageMod.applyId(other.criticalMalfunctionDamageMod, id)
	damageToCapital.applyId(other.damageToCapital, id)
	damageToCruisers.applyId(other.damageToCruisers, id)
	damageToDestroyers.applyId(other.damageToDestroyers, id)
	damageToFighters.applyId(other.damageToFighters, id)
	damageToFrigates.applyId(other.damageToFrigates, id)
	damageToMissiles.applyId(other.damageToMissiles, id)
	damageToTargetEnginesMult.applyId(other.damageToTargetEnginesMult, id)
	damageToTargetShieldsMult.applyId(other.damageToTargetShieldsMult, id)
	damageToTargetWeaponsMult.applyId(other.damageToTargetWeaponsMult, id)
	deceleration.applyId(other.deceleration, id)
	eccmChance.applyId(other.eccmChance, id)
	effectiveArmorBonus.applyId(other.effectiveArmorBonus, id)
	empDamageTakenMult.applyId(other.empDamageTakenMult, id)
	energyAmmoBonus.applyId(other.energyAmmoBonus, id)
	energyDamageTakenMult.applyId(other.energyDamageTakenMult, id)
	energyRoFMult.applyId(other.energyRoFMult, id)
	energyShieldDamageTakenMult.applyId(other.energyShieldDamageTakenMult, id)
	energyWeaponDamageMult.applyId(other.energyWeaponDamageMult, id)
	energyWeaponFluxCostMod.applyId(other.energyWeaponFluxCostMod, id)
	energyWeaponRangeBonus.applyId(other.energyWeaponRangeBonus, id)
	engineDamageTakenMult.applyId(other.engineDamageTakenMult, id)
	engineHealthBonus.applyId(other.engineHealthBonus, id)
	engineMalfunctionChance.applyId(other.engineMalfunctionChance, id)
	fighterRefitTimeMult.applyId(other.fighterRefitTimeMult, id)
	fighterWingRange.applyId(other.fighterWingRange, id)
	fluxCapacity.applyId(other.fluxCapacity, id)
	fluxDissipation.applyId(other.fluxDissipation, id)
	fragmentationDamageTakenMult.applyId(other.fragmentationDamageTakenMult, id)
	fragmentationShieldDamageTakenMult.applyId(other.fragmentationShieldDamageTakenMult, id)
	fuelMod.applyId(other.fuelMod, id)
	fuelUseMod.applyId(other.fuelUseMod, id)
	hangarSpaceMod.applyId(other.hangarSpaceMod, id)
	hardFluxDissipationFraction.applyId(other.hardFluxDissipationFraction, id)
	highExplosiveDamageTakenMult.applyId(other.highExplosiveDamageTakenMult, id)
	highExplosiveShieldDamageTakenMult.applyId(other.highExplosiveShieldDamageTakenMult, id)
	hitStrengthBonus.applyId(other.hitStrengthBonus, id)
	hullBonus.applyId(other.hullBonus, id)
	hullCombatRepairRatePercentPerSecond.applyId(other.hullCombatRepairRatePercentPerSecond, id)
	hullDamageTakenMult.applyId(other.hullDamageTakenMult, id)
	kineticArmorDamageTakenMult.applyId(other.kineticArmorDamageTakenMult, id)
	kineticDamageTakenMult.applyId(other.kineticDamageTakenMult, id)
	kineticShieldDamageTakenMult.applyId(other.kineticShieldDamageTakenMult, id)
	maxArmorDamageReduction.applyId(other.maxArmorDamageReduction, id)
	maxBurnLevel.applyId(other.maxBurnLevel, id)
	maxCombatHullRepairFraction.applyId(other.maxCombatHullRepairFraction, id)
	maxCombatReadiness.applyId(other.maxCombatReadiness, id)
	maxCrewMod.applyId(other.maxCrewMod, id)
	maxRecoilMult.applyId(other.maxRecoilMult, id)
	maxSpeed.applyId(other.maxSpeed, id)
	maxTurnRate.applyId(other.maxTurnRate, id)
	minArmorFraction.applyId(other.minArmorFraction, id)
	minCrewMod.applyId(other.minCrewMod, id)
	missileAccelerationBonus.applyId(other.missileAccelerationBonus, id)
	missileAmmoBonus.applyId(other.missileAmmoBonus, id)
	missileDamageTakenMult.applyId(other.missileDamageTakenMult, id)
	missileGuidance.applyId(other.missileGuidance, id)
	missileHealthBonus.applyId(other.missileHealthBonus, id)
	missileMaxSpeedBonus.applyId(other.missileMaxSpeedBonus, id)
	missileMaxTurnRateBonus.applyId(other.missileMaxTurnRateBonus, id)
	missileRoFMult.applyId(other.missileRoFMult, id)
	missileShieldDamageTakenMult.applyId(other.missileShieldDamageTakenMult, id)
	missileTurnAccelerationBonus.applyId(other.missileTurnAccelerationBonus, id)
	missileWeaponDamageMult.applyId(other.missileWeaponDamageMult, id)
	missileWeaponFluxCostMod.applyId(other.missileWeaponFluxCostMod, id)
	missileWeaponRangeBonus.applyId(other.missileWeaponRangeBonus, id)
	nonBeamPDWeaponRangeBonus.applyId(other.nonBeamPDWeaponRangeBonus, id)
	numFighterBays.applyId(other.numFighterBays, id)
	overloadTimeMod.applyId(other.overloadTimeMod, id)
	peakCRDuration.applyId(other.peakCRDuration, id)
	phaseCloakActivationCostBonus.applyId(other.phaseCloakActivationCostBonus, id)
	phaseCloakCooldownBonus.applyId(other.phaseCloakCooldownBonus, id)
	phaseCloakUpkeepCostBonus.applyId(other.phaseCloakUpkeepCostBonus, id)
	projectileDamageTakenMult.applyId(other.projectileDamageTakenMult, id)
	projectileShieldDamageTakenMult.applyId(other.projectileShieldDamageTakenMult, id)
	projectileSpeedMult.applyId(other.projectileSpeedMult, id)
	recoilDecayMult.applyId(other.recoilDecayMult, id)
	recoilPerShotMult.applyId(other.recoilPerShotMult, id)
	recoilPerShotMultSmallWeaponsOnly.applyId(other.recoilPerShotMultSmallWeaponsOnly, id)
	repairRatePercentPerDay.applyId(other.repairRatePercentPerDay, id)
	sensorProfile.applyId(other.sensorProfile, id)
	sensorStrength.applyId(other.sensorStrength, id)
	shieldAbsorptionMult.applyId(other.shieldAbsorptionMult, id)
	shieldArcBonus.applyId(other.shieldArcBonus, id)
	shieldDamageTakenMult.applyId(other.shieldDamageTakenMult, id)
	shieldMalfunctionChance.applyId(other.shieldMalfunctionChance, id)
	shieldMalfunctionFluxLevel.applyId(other.shieldMalfunctionFluxLevel, id)
	shieldTurnRateMult.applyId(other.shieldTurnRateMult, id)
	shieldUnfoldRateMult.applyId(other.shieldUnfoldRateMult, id)
	shieldUpkeepMult.applyId(other.shieldUpkeepMult, id)
	sightRadiusMod.applyId(other.sightRadiusMod, id)
	suppliesPerMonth.applyId(other.suppliesPerMonth, id)
	suppliesToRecover.applyId(other.suppliesToRecover, id)
	timeMult.applyId(other.timeMult, id)
	turnAcceleration.applyId(other.turnAcceleration, id)
	ventRateMult.applyId(other.ventRateMult, id)
	weaponDamageTakenMult.applyId(other.weaponDamageTakenMult, id)
	weaponHealthBonus.applyId(other.weaponHealthBonus, id)
	weaponMalfunctionChance.applyId(other.weaponMalfunctionChance, id)
	weaponRangeMultPastThreshold.applyId(other.weaponRangeMultPastThreshold, id)
	weaponRangeThreshold.applyId(other.weaponRangeThreshold, id)
	weaponTurnRateBonus.applyId(other.weaponTurnRateBonus, id)
	zeroFluxMinimumFluxLevel.applyId(other.zeroFluxMinimumFluxLevel, id)
	zeroFluxSpeedBoost.applyId(other.zeroFluxSpeedBoost, id)
}
// endregion

// region Vector2f Operators
operator fun Vector2f.plus(v: Vector2f): Vector2f {
	return Vector2f(x + v.x, y + v.y)
}

operator fun Vector2f.plusAssign(v: Vector2f) {
	x += v.x
	y += v.y
}

operator fun Vector2f.minus(v: Vector2f): Vector2f {
	return Vector2f(x - v.x, y - v.y)
}

operator fun Vector2f.minusAssign(v: Vector2f) {
	x -= v.x
	y -= v.y
}

operator fun Vector2f.unaryMinus(): Vector2f {
	return Vector2f(-x, -y)
}

operator fun Vector2f.times(v: Vector2f): Vector2f {
	return Vector2f(x * v.x, y * v.y)
}

operator fun Vector2f.times(f: Float): Vector2f {
	return Vector2f(x * f, y * f)
}

operator fun Vector2f.timesAssign(v: Vector2f) {
	x *= v.x
	y *= v.y
}

operator fun Vector2f.timesAssign(f: Float) {
	x *= f
	y *= f
}

operator fun Vector2f.div(v: Vector2f): Vector2f {
	return Vector2f(x / v.x, y / v.y)
}

operator fun Vector2f.div(f: Float): Vector2f {
	return Vector2f(x / f, y / f)
}

operator fun Vector2f.divAssign(v: Vector2f) {
	x /= v.x
	y /= v.y
}

operator fun Vector2f.divAssign(f: Float) {
	x /= f
	y /= f
}

/** Get euclidian distance between two vectors. */
operator fun Vector2f.rangeTo(v: Vector2f): Float {
	return sqrt((x - v.x).pow(2) + (y - v.y).pow(2))
}
// endregion
