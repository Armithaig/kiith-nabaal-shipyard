package nabaal.util

class ApatheticInterval(min: Float, max: Float, elapsed: Boolean = true) : Interval(min, max, elapsed) {
	constructor(time: Float, elapsed: Boolean = true) : this(time, time, elapsed)

	var min = min
		set(value) {
			field = value
			low = value
		}
	var max = max
		set(value) {
			field = value
			high = value
		}

	var active = 0.5f
	var passive = 3f
	var apathy = 1f
		set(value) {
			field = value.coerceIn(active, passive)
		}

	init {
		reset()
	}

	fun celerate() {
		low = min * apathy
		high = max * apathy
	}

	/** Report a success to the tracker, reducing apathy. */
	fun success() {
		apathy -= (apathy - active) / 8
		celerate()
	}

	/** Report a failure to the tracker, increasing apathy. */
	fun failure() {
		apathy += (passive - apathy) / 8
		celerate()
	}

	/** Report a success/failure to the tracker, reducing/increasing apathy. */
	fun report(success: Boolean) {
		if(success) success()
		else failure()
	}
}