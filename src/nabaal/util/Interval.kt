package nabaal.util

import kotlin.random.Random

open class Interval(min: Float, max: Float, elapsed: Boolean = true) {
	constructor(time: Float, elapsed: Boolean = true) : this(time, time, elapsed)

	var low = min
	var high = max

	var duration = 0f
	var time = 0f

	init {
		if (!elapsed) reset()
	}

	open fun reset() {
		duration = low + (high - low) * Random.nextFloat()
		time = 0f
	}

	open fun advance(amount: Float): Boolean {
		time += amount
		if (time >= duration) {
			reset()
			return true
		}
		return false
	}
}