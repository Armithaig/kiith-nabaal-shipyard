package nabaal.util

import com.fs.starfarer.api.Global
import com.fs.starfarer.api.combat.CombatEngineAPI
import com.fs.starfarer.api.util.Misc
import org.lwjgl.util.vector.Vector2f
import java.awt.Color

class SmokeGenerator {
	val combat: CombatEngineAPI = Global.getCombatEngine()
	var origin = Misc.ZERO
	var momentum = Misc.ZERO
	var size = Pair(0f, 0f)
	var color = Color(200, 200, 200, 127)
	var opacity: Float = 1f
	var duration = 2f
	var count = 8

	/** Generate smoke particles randomly within [radius]. */
	fun cloud(location: Vector2f, radius: Vector2f, size: Pair<Float, Float>, duration: Float, count: Int) {
		for (i in 1..count) {
			val rng = radius.range()
			val loc = origin + location
			val vel = momentum + rng
			combat.addSmokeParticle(loc, vel, size.first + size.second.range(false), opacity, duration, color)
		}
	}

	fun cloud(location: Vector2f, radius: Vector2f) {
		cloud(location, radius, size, duration, count)
	}

	/** Generate smoke particles moving in a line from [location] to [target]. */
	fun line(location: Vector2f, target: Vector2f, size: Pair<Float, Float>, duration: Float, count: Int) {
		for (i in 1..count) {
			val trg = target * (1f * i / count)
			val loc = origin + location + trg
			val vel = momentum + trg
			combat.addSmokeParticle(loc, vel, size.first + size.second.range(false), opacity, duration, color)
		}
	}

	fun line(location: Vector2f, target: Vector2f) {
		line(location, target, size, duration * 0.75f, count)
	}
}