package nabaal.hullmods

import com.fs.starfarer.api.combat.*
import com.fs.starfarer.api.combat.ShipAPI.HullSize


class HiigaranFleet : BaseHullMod() {
	companion object Constants {
		const val FLUX_BONUS_MULT = 1.10f
	}

	override fun applyEffectsBeforeShipCreation(hullSize: HullSize, stats: MutableShipStatsAPI, id: String) {
		stats.fluxCapacity.modifyMult(id, FLUX_BONUS_MULT)
		stats.fluxDissipation.modifyMult(id, FLUX_BONUS_MULT)
		stats.shieldArcBonus.modifyFlat(id, 10f)
	}
}
