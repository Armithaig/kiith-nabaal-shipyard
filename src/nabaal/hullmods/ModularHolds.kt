package nabaal.hullmods

import com.fs.starfarer.api.Global
import com.fs.starfarer.api.combat.*
import com.fs.starfarer.api.combat.ShipAPI.HullSize
import com.fs.starfarer.api.impl.campaign.ids.HullMods
import com.fs.starfarer.api.ui.TooltipMakerAPI
import com.fs.starfarer.api.util.Misc
import nabaal.util.lower
import kotlin.math.round


class ModularHolds : BaseHullMod() {
	enum class FreightType { CARGO, FUEL, CREW }
	data class Freight(val type: FreightType, val modType: FreightType, val amount: Float)

	companion object Constants {
		const val GFX_CAT = "nbl_ships"
		const val SLOT_ID = "CS 001"

		val mods = mapOf(
			HullMods.EXPANDED_CARGO_HOLDS to FreightType.CARGO,
			HullMods.AUXILIARY_FUEL_TANKS to FreightType.FUEL,
			HullMods.ADDITIONAL_BERTHING to FreightType.CREW
		)
	}

	val settings get() = Global.getSettings()

	override fun applyEffectsBeforeShipCreation(hullSize: HullSize, stats: MutableShipStatsAPI, id: String) {
		stats.apply {
			variant.clearSlot(SLOT_ID)

			val freight = getFreight(variant) ?: return

			when (freight.modType) {
				FreightType.CARGO -> cargoMod.modifyFlat(id, freight.amount)
				FreightType.FUEL -> fuelMod.modifyFlat(id, freight.amount)
				FreightType.CREW -> maxCrewMod.modifyFlat(id, freight.amount)
			}

			when (freight.type) {
				FreightType.CARGO -> cargoMod.modifyFlat(id, -freight.amount)
				FreightType.FUEL -> fuelMod.modifyFlat(id, -freight.amount)
				FreightType.CREW -> maxCrewMod.modifyFlat(id, -freight.amount)
			}

		}
	}

	override fun applyEffectsAfterShipCreation(ship: ShipAPI, id: String) {
		val freight = getFreight(ship.variant) ?: return
		val oldSprite = ship.spriteAPI
		val spriteName = ship.hullSpec.baseHullId + "_" + freight.modType.name.lower

		try {
			ship.setSprite(GFX_CAT, spriteName)
			ship.spriteAPI.setCenter(oldSprite.centerX, oldSprite.centerY)
		} catch (t: Throwable) {
			Global.getLogger(this::class.java).info("Missing sprite: $spriteName")
		}
	}

	override fun getDescriptionParam(index: Int, hullSize: HullSize?, ship: ShipAPI?): String? {
		return when (index) {
			0 -> settings.getHullModSpec(HullMods.EXPANDED_CARGO_HOLDS).displayName
			1 -> settings.getHullModSpec(HullMods.AUXILIARY_FUEL_TANKS).displayName
			2 -> settings.getHullModSpec(HullMods.ADDITIONAL_BERTHING).displayName
			else -> super.getDescriptionParam(index, hullSize, ship)
		}
	}

	override fun addPostDescriptionSection(
		tooltip: TooltipMakerAPI,
		hullSize: HullSize?,
		ship: ShipAPI?,
		width: Float,
		isForModSpec: Boolean
	) {
		if (ship == null) return

		getFreight(ship.variant)?.apply {
			val n = "%.0f".format(amount)
			tooltip.addPara(
				"Exchanges %s maximum %s capacity for maximum %s capacity.",
				10f,
				Misc.getHighlightColor(),
				n, type.name.lower, modType.name.lower
			).setHighlight(n)
		}
	}

	fun quintus(value: Float): Float {
		return round(value / 5f) * 5f
	}

	fun getFreight(variant: ShipVariantAPI): Freight? {
		val modType = variant.hullMods.firstOrNull { mods.containsKey(it) }?.let { mods[it] } ?: return null
		val freight = variant.hullSpec.run {
			if (cargo >= fuel && cargo >= maxCrew) Freight(FreightType.CARGO, modType, quintus(cargo * 0.75f))
			else if (fuel >= cargo && fuel >= maxCrew) Freight(FreightType.FUEL, modType, quintus(fuel * 0.75f))
			else Freight(FreightType.CREW, modType, quintus(maxCrew - minCrew * 2f))
		}

		return if (freight.type == modType) null else freight
	}

}
