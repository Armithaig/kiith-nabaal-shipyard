package nabaal.hullmods

import com.fs.starfarer.api.*
import com.fs.starfarer.api.combat.*
import com.fs.starfarer.api.combat.ShipAPI.HullSize
import com.fs.starfarer.api.impl.campaign.ids.Stats

// TODO: Consider reducing weapon range here like safety override
//  High speed + long range'll probably be crabby
//  Would be mutually exclusive with Reinforced Hull if possible, farks up the autofitter if ya just remove/suppress it
class CorvetteHull : BaseHullMod() {
	companion object {
		val dmods: List<String> by lazy {
			Global.getSettings().allHullModSpecs
				.filter { it.hasTag("dmod") }
				.map { it.id }
		}
	}

	override fun applyEffectsBeforeShipCreation(hullSize: HullSize, stats: MutableShipStatsAPI, id: String) {
		stats.apply {
			dynamic.getMod(Stats.INDIVIDUAL_SHIP_RECOVERY_MOD).modifyFlat(id, 1000f)
			dynamic.getMod(Stats.INSTA_REPAIR_FRACTION).modifyFlat(id, 0.25f)
			breakProb.modifyMult(id, 0f)
			suppliesToRecover.modifyMult(id, 0.5f)

			// Make immune to D-mods
			// Woulda preferred just reducing chance with somethin' like Stats.INDIVIDUAL_SHIP_DMOD_REDUCTION
			variant.suppressedMods.addAll(dmods)
			variant.hullMods.removeAll(dmods)
		}
	}

	override fun applyEffectsAfterShipCreation(ship: ShipAPI, id: String) {
		//ship.hullSize = HullSize.FIGHTER
		ship.collisionClass = CollisionClass.FIGHTER
	}

	/*override fun getUnapplicableReason(ship: ShipAPI): String {
		val mod = ship.variant.hullMods.firstOrNull { precludes.contains(it) }
		if (mod != null) return "Incompatible with " + settings.getHullModSpec(mod)?.displayName
		return "Ship hull is too small"
	}*/
}
