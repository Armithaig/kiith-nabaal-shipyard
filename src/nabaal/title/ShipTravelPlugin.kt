package nabaal.title

import com.fs.starfarer.api.*
import com.fs.starfarer.api.combat.*
import com.fs.starfarer.api.input.InputEventAPI
import com.fs.starfarer.combat.entities.Ship
import nabaal.combat.ai.ZeroAI
import nabaal.util.*
import org.lwjgl.util.vector.Vector2f
import kotlin.concurrent.thread

// Forcibly spawn some ships on boot title-screen to trigger class loads
class ShipTravelPlugin : BaseEveryFrameCombatPlugin() {
	companion object {
		val settings = Global.getSettings()

		// Start a thread to poll for the title combat engine to initialise and shim our plugin in
		fun start() {
			thread(start = true, isDaemon = true, contextClassLoader = settings.scriptClassLoader) {
				while (Global.getCurrentState() != GameState.CAMPAIGN) {
					val combat = Global.getCombatEngine()
					try {
						if (combat?.ships?.isNotEmpty() == true) {
							combat.addPlugin(ShipTravelPlugin())
							break
						}
						Thread.sleep(50)
					} catch (e: ConcurrentModificationException) {
						// This s'what passes for thread safety 'round east
						Global.getLogger(this::class.java).info("Failed adding title plugin, retrying")
					}
				}
			}
		}
	}

	lateinit var combat: CombatEngineAPI
	lateinit var viewport: ViewportAPI
	val variants = mutableListOf("vgr_destroyer_a", "hgn_pulsarcorvette_a")
	val ships = mutableListOf<ShipAPI>()

	override fun init(engine: CombatEngineAPI) {
		combat = engine
		viewport = engine.viewport
	}

	override fun advance(amount: Float, events: MutableList<InputEventAPI>?) {
		if (variants.isEmpty() && ships.isEmpty()) return combat.removePlugin(this)

		ships.removeAll { ship ->
			if (viewport.isNearViewport(ship.location, ship.spriteAPI.height + viewport.visibleHeight)) {
				engineGlow(ship)
				ship.deployedDrones?.forEach { engineGlow(it) }
				return@removeAll false
			} else {
				ship.deployedDrones?.forEach { combat.removeObject(it) }
				combat.removeObject(ship)
				return@removeAll true
			}
		}

		variants.removeAll { id ->
			val fleet = combat.getFleetManager(0)
			val variant = settings.getVariant(id)
			val sprite = settings.getSprite(variant.hullSpec.spriteName)
			val location = Vector2f(
				viewport.llx + viewport.visibleWidth.range(false),
				viewport.lly - sprite.height - viewport.visibleHeight.range(false)
			)

			val facing = location.angle(viewport.center) + 22.5f.range()
			val ship = fleet.spawnShipOrWing(variant.hullVariantId, location, facing, 0f)
			mutate(ship)

			ship.shipAI = ZeroAI(ship as Ship).apply { engine.setDesiredHeading(facing, 1f) }
			ship.deployedDrones?.forEach {
				// Just nuke 'em for now, inconsistent gfx engine glitch on drones
				combat.removeObject(it)
				/*if (it is Ship) {
					it.resetParentAI()
					it.setLoc(Vector2f(location))
				}
				mutate(it)*/
			}
			ships.add(ship)
		}
	}

	fun engineGlow(ship: ShipAPI) {
		val controller = ship.engineController
		controller.extendFlame(this, 2f, 1.5f, 0.25f)
		controller.shipEngines.forEach {
			controller.setFlameLevel(it.engineSlot, 1f)
		}
	}

	fun mutate(ship: ShipAPI) {
		ship.mutableStats.maxSpeed.modifyFlat("title_screen", 150f)
		ship.mutableStats.maxSpeed.modifyMult("title_screen", 0.5f)
		//ship.mutableStats.acceleration.modifyPercent("title_screen", -100f)
		//ship.mutableStats.acceleration.modifyFlat("title_screen", 5f)
		ship.collisionClass = CollisionClass.NONE
	}
}