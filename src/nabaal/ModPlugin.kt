package nabaal

import com.fs.starfarer.api.BaseModPlugin
import nabaal.title.ShipTravelPlugin

class ModPlugin : BaseModPlugin() {

	override fun onApplicationLoad() {
		ShipTravelPlugin.start()
	}

}
