package nabaal.shipsystems

import com.fs.starfarer.api.Global
import com.fs.starfarer.api.combat.*
import com.fs.starfarer.api.impl.combat.BaseShipSystemScript
import com.fs.starfarer.api.plugins.ShipSystemStatsScript.*
import com.fs.starfarer.combat.ai.missile.MirvAI
import nabaal.util.*
import java.awt.Color
import kotlin.random.Random


// TODO: Write missile AI capable of targeting other missiles
class MissileDefenseStats : BaseShipSystemScript() {
	companion object Constants {
		const val SYSTEM_ID = "nbl_missile_defense"
		const val RANGE = 1500f
		const val BURST_SIZE = 4
		const val BURST_DELAY = 0.3f
		const val MIN_STRAIN = 0.01f
		const val MAX_STRAIN = 0.125f
	}

	lateinit var ship: ShipAPI
	var targets = listOf<ShipTarget>()
		get() {
			if (field.isEmpty())
				field = ship.selectShips(BURST_SIZE, RANGE) { it.isFighter && it.owner != ship.owner }

			return field
		}
	var strain = MIN_STRAIN
		set(value) {
			field = value.coerceIn(MIN_STRAIN, MAX_STRAIN)
		}

	val wpn by lazy { ship.allWeapons.first { it.spec.tags.contains(SYSTEM_ID) } }
	val combat = Global.getCombatEngine()
	var bursts = 0
	var interval = Interval(BURST_DELAY)

	override fun apply(stats: MutableShipStatsAPI, id: String, state: State?, effect: Float) {
		if (bursts >= BURST_SIZE)
			return ship.system.deactivate()

		if (!interval.advance(combat.elapsedInLastFrame) || wpn.chargeLevel >= 1f) return
		if (misfire()) return

		wpn.setRemainingCooldownTo(wpn.cooldownRemaining.coerceAtLeast(BURST_DELAY))
		val offset = wpn.location + wpn.spec.hardpointFireOffsets.modGet(bursts).rotate(wpn.currAngle)
		val missile = wpn.fireMissile(SYSTEM_ID, offset)
		if (targets.isNotEmpty()) {
			val ai = missile.ai as MirvAI
			ai.target = targets.modGet(bursts).ship
		}

		bursts++
	}

	var last = 0f
	fun misfire(): Boolean {
		if (bursts == 0) {
			val elapsed = combat.getTotalElapsedTime(false)
			val diff = (elapsed - last) / 15f
			last = elapsed

			strain *= if (diff > 1f) 1f - diff * 0.25f
			else 1.5f
		}

		if (strain >= MIN_STRAIN * 2 && Random.nextFloat() < strain) {
			ship.system.deactivate()
			wpn.disable()
			strain = MIN_STRAIN
			combat.addFloatingText(wpn.location, "Autoforge Malfunction", 20f, Color.BLUE, ship, 1f, 0f)
			return true
		}

		return false
	}

	override fun unapply(stats: MutableShipStatsAPI?, id: String?) {
		bursts = 0
		interval.duration = 0f
		targets = if (targets.isEmpty()) targets else listOf()
	}

	override fun getUsesOverride(ship: ShipAPI): Int {
		this.ship = ship
		return ship.mutableStats.missileAmmoBonus.computeEffective(3f).toInt()
	}

	override fun isUsable(system: ShipSystemAPI, ship: ShipAPI): Boolean {
		if (system.isOn || wpn.isDisabled || ship.fluxTracker.isOverloadedOrVenting)
			return false

		return super.isUsable(system, ship)
	}

	override fun getStatusData(index: Int, state: State?, effectLevel: Float): StatusData? {
		if (index != 0) return null
		return StatusData("remaining: " + (4 - bursts), false)
	}

}
