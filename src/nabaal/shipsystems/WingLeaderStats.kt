package nabaal.shipsystems

import com.fs.starfarer.api.combat.*
import com.fs.starfarer.api.combat.DroneLauncherShipSystemAPI.DroneOrders
import com.fs.starfarer.api.combat.ShipAPI.HullSize
import com.fs.starfarer.api.impl.combat.BaseShipSystemScript
import com.fs.starfarer.api.plugins.ShipSystemStatsScript.State
import com.fs.starfarer.combat.CombatEngine
import com.fs.starfarer.combat.entities.Ship
import nabaal.combat.ai.*
import nabaal.util.applyId
import org.lwjgl.util.vector.Vector2f


// TODO: Write wingman AI so we can bypass drone system -> spawn own ships based on mothership -> one class for all wing systems.
//  Could leverage ship modules to allow customisation of the entire wing.
class WingLeaderStats : BaseShipSystemScript() {
	companion object Constants {
		const val SYSTEM_TAG = "nbl_wing_leader"
	}

	lateinit var ship: ShipAPI
	val launcher by lazy { ship.system as DroneLauncherShipSystemAPI }
	val maxDrones by lazy { launcher.specJson.getInt("maxDrones") }
	val lights by lazy {
		var i = 0
		return@lazy ship.allWeapons.filter {
			if (!it.spec.hasTag(SYSTEM_TAG)) return@filter false
			it.animation.alphaMult = 0f
			it.animation.frame = i++ % it.animation.numFrames
			return@filter true
		}
	}

	val inapt get() = ship.isHulk || ship.originalOwner < 0 || ship.isDrone
	val orders get() = launcher.droneOrders
	val combat = CombatEngine.getInstance()
	var drones = hashSetOf<String>()

	override fun apply(stats: MutableShipStatsAPI, id: String, state: State, effect: Float) {
		if (inapt) return

		evalEffect(id, effect)
		evalOrders()
	}

	var lastOrders: DroneOrders? = null
	fun evalOrders() {
		if (orders == lastOrders) return

		if (orders == DroneOrders.RECALL) {
			ship.deployedDrones.forEach {
				if (it.isHulk) return@forEach
				it.abortLanding()
				it.shipAI = FullRetreatAI(it as Ship) {
					// Trigger an instant landing
					it.beginLandingAnimation(ship)
					it.sinceLaunch = 0f
					drones.remove(it.id)
				}
			}
		} else {
			if (lastOrders == DroneOrders.RECALL) {
				ship.deployedDrones.forEach {
					if (it.isHulk) return@forEach

					val ai = it.shipAI
					if (ai is TransientAI) ai.yield()
				}
			}
		}
		lastOrders = orders
	}

	var lastEffect = -1f
	fun evalEffect(id: String, effect: Float) {
		if (effect == lastEffect) return
		lights.forEach { it.animation.alphaMult = effect / maxDrones }

		if (effect == 0f)
			drones.clear()

		if (orders != DroneOrders.RECALL) {
			// Spawn new drones in gracefully.
			ship.deployedDrones.forEach {
				if (it.isHulk || !drones.add(it.id)) return@forEach

				val ftr = it as Ship
				val offset = launcher.getIndex(ftr) * 1f - (maxDrones - 1) / 2
				applyEffectsToDrone(ftr, id)
				ftr.hullSize = HullSize.FIGHTER // Circumvents collision freakout by leader
				ftr.sinceLaunch = 5f
				ftr.shipAI = SpawnAI(ftr, Vector2f(if (offset == 0f) 100f else 0f, 200f * offset), 3f)
			}
		}

		lastEffect = effect
	}

	// Nick all mutable stats gained from hull mods/vents/capacitors.
	// Dynamic stats ignored, s' mostly campaign/fleet wide effects there.
	// TODO: Inherit hullmods directly to variant prior to ship spawn when able (Custom Wing AI)
	fun applyEffectsToDrone(drone: ShipAPI, id: String) {
		ship.variant.hullMods.union(listOf("flux_capacitors", "flux_vents")).forEach {
			drone.mutableStats.applyId(ship.mutableStats, it)
		}
		drone.currentCR = ship.currentCR
	}

	// Faux init
	override fun getUsesOverride(ship: ShipAPI): Int {
		this.ship = ship
		// Should be early enough to be at/near top of command queue when deployed
		ship.useSystem()
		return super.getUsesOverride(ship)
	}

	override fun isUsable(system: ShipSystemAPI, ship: ShipAPI): Boolean {
		return !inapt
	}

}
