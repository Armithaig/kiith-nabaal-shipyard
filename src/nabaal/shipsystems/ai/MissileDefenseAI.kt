package nabaal.shipsystems.ai


import com.fs.starfarer.api.combat.*
import com.fs.starfarer.api.combat.ShipwideAIFlags.AIFlags
import com.fs.starfarer.api.impl.campaign.ids.Personalities
import nabaal.shipsystems.MissileDefenseStats.Constants.BURST_SIZE
import nabaal.shipsystems.MissileDefenseStats.Constants.RANGE
import nabaal.shipsystems.MissileDefenseStats.Constants.SYSTEM_ID
import nabaal.util.*
import org.lwjgl.util.vector.Vector2f
import kotlin.random.Random


class MissileDefenseAI : ShipSystemAIScript {

	lateinit var ship: ShipAPI
	lateinit var wpn: WeaponAPI
	lateinit var combat: CombatEngineAPI
	//lateinit var flags: ShipwideAIFlags
	lateinit var system: ShipSystemAPI

	val interval = ApatheticInterval(0.5f, 1f).apply { passive = 5f }

	override fun init(ship: ShipAPI, system: ShipSystemAPI, flags: ShipwideAIFlags, combat: CombatEngineAPI) {
		this.ship = ship
		this.wpn = ship.allWeapons.first { it.spec.tags.contains(SYSTEM_ID) }
		this.combat = combat
		//this.flags = flags
		this.system = system
	}

	override fun advance(amount: Float, missileDangerDir: Vector2f?, collisionDangerDir: Vector2f?, target: ShipAPI?) {
		if (!interval.advance(amount)) return

		val state = ship.systemState
		if (state.ordinal > 5 || wpn.isDisabled) return interval.failure()
		if (state.ordinal > 0) return

		val fighters = ship.selectShips(BURST_SIZE, RANGE) { it.isFighter && it.owner != ship.owner }
		if (fighters.isNotEmpty())
			return evalFighters(fighters)
		else if (target != null && ship.location..target.location <= RANGE)
			return evalTarget(target)
	}

	fun evalTarget(target: ShipAPI) {
		var chance = 1f
		var minAmmo = system.maxAmmo.toFloat()
		when (ship.captain?.personalityAPI?.id) {
			Personalities.AGGRESSIVE -> minAmmo *= 0.75f
			Personalities.RECKLESS -> minAmmo *= 0.5f
		}

		chance *= (system.ammo / minAmmo).coerceIn(0f, 1f)
		chance *= if (target.fluxTracker.isOverloadedOrVenting) 1.25f else target.fluxTracker.fluxLevel
		chance *= 1f - target.closestArmorCell(wpn.location).fraction

		if (chance >= Random.nextFloat())
			ship.useSystem()

		interval.report(chance >= 0.5f)
	}

	fun evalFighters(fighters: List<ShipTarget>) {
		var chance = fighters.size.toFloat() / BURST_SIZE

		fighters.forEach {
			val wing = it.ship.wing ?: return@forEach
			if (wing.spec.isBomber)
				chance *= 1.1f
			else if (wing.spec.isInterceptor)
				chance *= 0.9f

			if (wing.leader.aiFlags.hasFlag(AIFlags.IN_ATTACK_RUN))
				chance *= 1.1f
		}

		when (ship.captain?.personalityAPI?.id) {
			Personalities.AGGRESSIVE -> chance *= 1.1f
			Personalities.RECKLESS -> chance *= 1.25f
		}

		if (chance >= Random.nextFloat())
			ship.useSystem()

		interval.success()
	}

}
