package nabaal.shipsystems.ai


import com.fs.starfarer.api.combat.*
import com.fs.starfarer.api.impl.campaign.ids.Personalities
import com.fs.starfarer.combat.entities.Ship
import nabaal.shipsystems.RamDriveStats.Constants.MAX_ARC
import nabaal.shipsystems.RamDriveStats.Constants.MAX_RANGE
import nabaal.shipsystems.RamDriveStats.Constants.MIN_RANGE
import nabaal.util.*
import org.lwjgl.util.vector.Vector2f
import kotlin.math.abs
import kotlin.random.Random

// Instantiated and ticked in RamDriveStats to preserve BURN_DRIVE ai.
class RamDriveAI : ShipSystemAIScript {

	lateinit var ship: ShipAPI
	lateinit var combat: CombatEngineAPI
	//lateinit var flags: ShipwideAIFlags
	lateinit var system: ShipSystemAPI

	val interval = Interval(0.5f, 1f)
	var target: CombatEntityAPI? = null
		get() = field.also { field = null }


	override fun init(ship: ShipAPI, system: ShipSystemAPI, flags: ShipwideAIFlags?, combat: CombatEngineAPI) {
		this.ship = ship as Ship
		this.target = ship
		this.combat = combat
		//this.flags = flags
		this.system = system
	}

	fun advance(amount: Float, target: ShipAPI? = null) {
		advance(amount, null, null, target)
	}

	override fun advance(amount: Float, missileDangerDir: Vector2f?, collisionDangerDir: Vector2f?, target: ShipAPI?) {
		if (!interval.advance(amount)) return
		if (ship.systemState.ordinal > 0) return

		val trg = ship.selectShips(MAX_RANGE) {
			it.collisionClass == ship.collisionClass && abs(ship.toFace(it.location)) <= MAX_ARC
		}

		if (trg == null || trg.range < MIN_RANGE) return
		var chance = MAX_RANGE / trg.range * 0.5f

		trg.ship.apply {
			// Closest ship in arc's friendly, don't ram it.
			if (owner == ship.owner) return

			chance *= when {
				shield == null -> 2f
				fluxTracker.overloadTimeRemaining + fluxTracker.timeToVent > 5f -> 1.75f
				else -> fluxLevel + 0.5f
			}
			chance *= mutableStats.armorDamageTakenMult.modified
			chance *= 1.5f - ship.fluxTracker.fluxLevel

			when (ship.captain?.personalityAPI?.id) {
				Personalities.AGGRESSIVE -> chance *= 1.25f
				Personalities.RECKLESS -> chance *= 1.5f
			}
		}

		if (chance >= Random.nextFloat()) {
			this.target = trg.ship
			ship.useSystem()
		}
	}

}
