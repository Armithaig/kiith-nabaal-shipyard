package nabaal.shipsystems

import com.fs.starfarer.api.Global
import com.fs.starfarer.api.combat.*
import com.fs.starfarer.api.impl.combat.BaseShipSystemScript
import com.fs.starfarer.api.loading.DamagingExplosionSpec
import com.fs.starfarer.api.plugins.ShipSystemStatsScript.*
import com.fs.starfarer.api.util.Misc
import com.fs.starfarer.combat.ai.movement.*
import com.fs.starfarer.combat.entities.Ship
import nabaal.combat.ai.*
import nabaal.shipsystems.ai.RamDriveAI
import nabaal.util.*
import java.awt.Color
import kotlin.math.max
import kotlin.random.Random

class RamDriveStats : BaseShipSystemScript() {
	companion object Constants {
		// TODO: load from .system file spec
		const val SYSTEM_ID = "nbl_ram_drive"
		const val SLOT_PREFIX = "RS"
		const val MAX_SPEED = 600f
		const val MAX_ACCEL = MAX_SPEED * 2f
		const val MAX_RANGE = MAX_SPEED
		const val MIN_RANGE = 300f
		const val MAX_ARC = 10f

		val color = Color(255, 75, 75, 55)
		val contrail = Color(255, 75, 75, 25)
		val blocked = listOf(
			ShipCommand.STRAFE_LEFT,
			ShipCommand.STRAFE_RIGHT,
			ShipCommand.ACCELERATE,
			ShipCommand.ACCELERATE_BACKWARDS,
			ShipCommand.DECELERATE,
			ShipCommand.VENT_FLUX,
			ShipCommand.TOGGLE_SHIELD_OR_PHASE_CLOAK,
			ShipCommand.USE_SYSTEM,
			ShipCommand.FIRE,
			ShipCommand.HOLD_FIRE
		)
	}

	lateinit var ship: ShipAPI
	lateinit var controller: ShipEngineControllerAPI
	var baseMass = Float.NaN

	val slots by lazy { ship.hullSpec.allWeaponSlotsCopy.filter { it.id.startsWith(SLOT_PREFIX) && it.isSystemSlot } }
	val combat = Global.getCombatEngine()
	val sound = Global.getSoundPlayer()
	var ai: RamDriveAI? = null
	var arms: List<MissileAPI>? = null
	var collision = false

	override fun apply(stats: MutableShipStatsAPI, id: String, state: State, effect: Float) {
		// Work around quirk in BURN_DRIVE ai to suddenly send ACTIVE & 1f on init
		if (lastState.ordinal > 2 && effect == 1f) return

		if (state.ordinal > 2) {
			if (ai == null && ship.shipAI != null)
				ai = RamDriveAI().also { it.init(ship, ship.system, ship.aiFlags, combat) }
			else if (ai != null && ship.shipAI == null)
				ai = null

			ai?.advance(combat.elapsedInLastFrame, ship.shipTarget)
			return evalState(stats, id, state)
		}

		val active = if (state != State.IN) effect
		else max((effect - 0.5f) * 2f, 0f)

		val case = if (state.ordinal < 2 && collision) State.OUT
		else if (state.ordinal < 1 && active > 0f) State.ACTIVE
		else state

		evalState(stats, id, case)
		evalEffect(stats, id, effect, active)
		evalProj()
		everyFrame(case, effect, active)
	}

	var lastState = State.IDLE
	fun evalState(stats: MutableShipStatsAPI, id: String, state: State) {
		if (state == lastState) return

		when (state) {
			State.IN -> {
				ai?.target?.also { if (ship.ai != null) ship.shipAI = RammingAI(ship, it) }
				stats.maxSpeed.modifyMult(id, 0.1f)
				ship.shield?.toggleOff()
				ship.system
			}
			State.ACTIVE -> {
				if (misfire()) return
				stats.maxSpeed.unmodifyMult(id)
				stats.maxSpeed.modifyFlat(id, MAX_SPEED);
				stats.acceleration.modifyFlat(id, MAX_ACCEL)
			}
			State.OUT -> {
				ship.ai?.let { if (it is RammingAI) it.yield() }
				stats.maxSpeed.unmodifyFlat(id)
				stats.acceleration.modifyFlat(id, MAX_ACCEL / 2f)
				sound.playSound(SYSTEM_ID + "_deactivate", 1f, 1f, ship.location, ship.velocity)
			}
			else -> {
				stats.maxTurnRate.unmodify(id)
				stats.maxSpeed.unmodify(id)
				stats.acceleration.unmodify(id)
				stats.effectiveArmorBonus.unmodify(id)
				stats.armorDamageTakenMult.unmodify(id)
				arms?.forEach { combat.removeObject(it) }
				arms = null
				collision = false
				ship.mass = baseMass
			}
		}

		lastState = state
	}

	var lastEffect = 0f
	fun evalEffect(stats: MutableShipStatsAPI, id: String, effect: Float, active: Float) {
		if (effect == lastEffect) return

		val mult = 1f - effect * 0.9f
		stats.maxTurnRate.modifyMult(id, mult)
		stats.hullDamageTakenMult.modifyMult(id, mult)
		stats.armorDamageTakenMult.modifyMult(id, mult)
		stats.empDamageTakenMult.modifyMult(id, mult)
		ship.mass = baseMass + baseMass * active * 2f

		lastEffect = effect
	}

	fun evalProj() {
		ship.apply {
			// TODO: Consider spawning bombs early n' using arming time instead to give AI more time to react
			if (arms == null && velocity.length() > MAX_SPEED * 0.6f) {
				arms = slots.map {
					combat.spawnProjectile(this, null, SYSTEM_ID, location, facing, velocity) as MissileAPI
				}
			}

			arms?.forEachIndexed { i, it ->
				if (it.didDamage()) {
					collision = true
					return@forEachIndexed
				}

				it.location.set(location + slots[i].location.rotate(facing))
				it.velocity.set(velocity)
				it.facing = facing
			}
		}
	}

	fun everyFrame(state: State, effect: Float, active: Float) {
		if (state == State.IN) {
			sound.playLoop(SYSTEM_ID + "_loop", ship, 0.35f - effect * 0.2f, 1f, ship.location, ship.velocity)
			controller.extendFlame(this, -effect, 0f, -effect)
		} else if (state == State.ACTIVE) {
			sound.playLoop(SYSTEM_ID + "_loop", ship, 0.25f + active * 0.75f, 1f, ship.location, ship.velocity)
			controller.extendFlame(this, active * 2f, active * 2f, active * 2f)
		}
		controller.fadeToOtherColor(this, color, contrail, effect, 0.75f)
		blocked.forEach { ship.blockCommandForOneFrame(it) }
		// Blocking fire here's ineffective, assume system stat scripts're ticked after weapons
		ship.isHoldFireOneFrame = true
	}

	fun misfire(): Boolean {
		if (Random.nextFloat() > 0.01f) return false
		val loc = controller.shipEngines.maxBy { it.engineSlot.run { width + length } }?.location ?: return false
		val neg = Misc.getNegativeHighlightColor()
		val spec = DamagingExplosionSpec(
			0.1f,
			65f,
			65f,
			500f,
			250f,
			CollisionClass.HITS_SHIPS_AND_ASTEROIDS,
			CollisionClass.HITS_SHIPS_AND_ASTEROIDS,
			5f,
			3f,
			1f,
			150,
			Color(255, 165, 135, 255),
			Color(255, 125, 80, 255)
		).apply {
			damageType = DamageType.HIGH_EXPLOSIVE
			soundSetId = "nbl_ram_drive_misfire"
		}

		combat.spawnDamagingExplosion(spec, ship, loc, true)
		controller.forceFlameout(true)
		ship.system.deactivate()
		combat.addFloatingText(loc, "Drive Malfunction", 20f, neg, ship, 1f, 0f)
		return true
	}

	override fun getUsesOverride(ship: ShipAPI): Int {
		this.ship = ship
		controller = ship.engineController
		baseMass = ship.mass
		return super.getUsesOverride(ship)
	}

	override fun getStatusData(index: Int, state: State, effect: Float): StatusData? {
		if (state.ordinal > 2) return null

		return when (index) {
			0 -> if (effect > 0.5f) StatusData("increased engine power", false) else null
			1 -> StatusData("%.0f%% less damage taken".format(effect * 90f), false)
			else -> null
		}
	}

	// Would've liked for it to set up ramming opportunities independently but'd take rewriting the entire AI stack.
	private class RammingAI(shipAPI: ShipAPI, var target: CombatEntityAPI) : TransientShipAI {
		override var ship = shipAPI as Ship
		override var parent = ship.resetParentAI()
		var engine: EngineAI = parent.motorAI ?: BasicEngineAI(ship)

		override fun advance(amount: Float) {
			val angle = ship.location.angle(target.location + target.velocity)
			engine.setDesiredHeading(angle, 1f)
			engine.setDesiredFacing(angle)
			engine.advance(amount)
		}
	}
}
