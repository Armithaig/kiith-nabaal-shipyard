package nabaal.combat.ai

import com.fs.starfarer.api.Global
import com.fs.starfarer.api.combat.*
import com.fs.starfarer.combat.ai.movement.*
import com.fs.starfarer.combat.entities.Ship
import nabaal.util.Interval
import kotlin.math.abs

/** Retreats off the map at travel speed, invoking [callback] when out of bounds.
 * [Callback][callback] should ideally remove the ship from play.
 * */
class FullRetreatAI(
	shipAPI: ShipAPI,
	var facing: Float = Float.NaN,
	engineAI: EngineAI? = null,
	var callback: () -> Unit = {}
) : TransientShipAI {

	override var ship = shipAPI as Ship
	override var parent = ship.resetParentAI()
	var engine: EngineAI = engineAI ?: parent.motorAI ?: BasicEngineAI(ship)
	var combat: CombatEngineAPI = Global.getCombatEngine()
	var interval = Interval(0.5f, 1f)
	var called = false

	init {
		if (facing.isNaN()) {
			facing = when (ship.owner) {
				1 -> 90f
				2 -> 0f
				3 -> 180f
				else -> 270f
			}
		}

		// Ignore map bounds instead of setting retreat, callback'll handle ship despawn appropriately.
		ship.isIgnoresMapBounds = true
		engine.setDesiredFacing(facing)
		engine.setDesiredHeading(facing, 1f)
	}

	override fun advance(amount: Float) {
		engine.advance(amount)

		if (!interval.advance(amount)) return
		if (!ship.isTravelDriveOn && abs(facing - ship.facing) < 3f)
			ship.turnOnTravelDrive()
		else if (ship.isTravelDriveOn && abs(facing - ship.facing) > 5f)
			ship.turnOffTravelDrive()


		if (called) return
		if (abs(ship.location.x) > combat.mapWidth / 2 || abs(ship.location.y) > combat.mapHeight / 2) {
			callback()
			called = true
		}
	}

	override fun yield() {
		super.yield()
		ship.isIgnoresMapBounds = ship.isFighter
		ship.turnOffTravelDrive()
	}
}
