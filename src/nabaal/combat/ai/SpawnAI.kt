package nabaal.combat.ai

import com.fs.starfarer.api.Global
import com.fs.starfarer.api.combat.*
import com.fs.starfarer.combat.CombatEngine
import com.fs.starfarer.combat.ai.movement.*
import com.fs.starfarer.combat.entities.Ship
import nabaal.util.*
import org.lwjgl.util.vector.Vector2f
import kotlin.math.min

/** Flies in at travel speed for [duration] before invoking [callback] and yielding to previous AI. */
class SpawnAI(
	shipAPI: ShipAPI,
	goal: Vector2f = Vector2f(0f, 0f),
	val duration: Float = 5f,
	var facing: Float = Float.NaN,
	engineAI: EngineAI? = null,
	var callback: () -> Unit = {}
) : TransientShipAI {

	companion object Constants {
		const val TRAVEL_ID = "traveldrive_spawn"
		const val TRAVEL_SPEED = 600f
	}

	override var ship = shipAPI as Ship
	override var parent = ship.resetParentAI()
	var engine: EngineAI = engineAI ?: parent.motorAI ?: BasicEngineAI(ship)
	val controller = (ship as ShipAPI).engineController
	var travelDrive = false
	val combat = CombatEngine.getInstance()
	val sound = Global.getSoundPlayer()
	var elapsed = 0f

	init {
		if (facing.isNaN()) {
			facing = when (ship.owner) {
				1 -> 270f
				2 -> 180f
				3 -> 0f
				else -> 90f
			}
		}

		val speed = ship.maxSpeed + TRAVEL_SPEED * 0.75f
		ship.setApplyExtraAlphaToEngines(true)
		ship.setExtraAlphaMult(0f)
		ship.facing = facing
		ship.location -= (goal + Vector2f(speed * duration, 0f)).rotate(facing)
		ship.velocity.set(Vector2f(speed, 0f).rotate(facing))
		engine.setDesiredFacing(facing)
		engine.setDesiredHeading(Float.MAX_VALUE, 1f)
	}

	override fun advance(amount: Float) {
		engine.advance(amount)
		elapsed += amount

		val fraction = elapsed / duration
		ship.setExtraAlphaMult(min(1f, fraction * 2))
		controller.shipEngines.forEach { controller.setFlameLevel(it.engineSlot, 1f) }

		if (!travelDrive && fraction < 0.75f) {
			ship.mutableStats.maxSpeed.modifyFlatAlways(TRAVEL_ID, TRAVEL_SPEED, null)
			travelDrive = true
		}

		if (travelDrive) {
			if (fraction >= 0.75f) {
				ship.mutableStats.maxSpeed.unmodifyFlat(TRAVEL_ID)
				sound.playSound("system_travel_drive_deactivate_fix_zc", 1f, 1f, ship.location, ship.velocity)
				travelDrive = false
			} else {
				engineGlow()
				ShipCommand.values().forEach { ship.blockCommandForOneFrame(it) }
				sound.playLoop("system_travel_drive_loop", ship, 1f, 1f, ship.location, ship.velocity)
			}
		}

		if (elapsed >= duration) yield()
	}

	fun engineGlow() {
		controller.extendFlame(this, 4f, 3f, 2f)
		controller.extendLengthFraction.advance(1f)
		controller.extendWidthFraction.advance(1f)
		controller.extendGlowFraction.advance(1f)
	}

	override fun yield() {
		super.yield()
		ship.setExtraAlphaMult(1f)
		callback()
	}

}
