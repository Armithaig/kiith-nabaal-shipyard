package nabaal.combat.ai

import com.fs.starfarer.api.combat.ShipAPI
import com.fs.starfarer.combat.ai.movement.*
import com.fs.starfarer.combat.entities.Ship

class ZeroAI(shipAPI: ShipAPI, engineAI: EngineAI? = null) : TransientShipAI {
	override var ship = shipAPI as Ship
	override var parent = ship.resetParentAI()
	var engine: EngineAI = engineAI ?: parent.motorAI ?: BasicEngineAI(ship)

	override fun advance(p0: Float) {
		engine.advance(p0)
	}
}
