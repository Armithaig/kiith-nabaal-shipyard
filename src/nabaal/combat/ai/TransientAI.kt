package nabaal.combat.ai

import com.fs.starfarer.api.combat.CombatEntityAPI
import com.fs.starfarer.combat.ai.*
import com.fs.starfarer.combat.ai.movement.EngineAI
import com.fs.starfarer.combat.ai.system.drones.DroneAI

/** Short duration AI employed for a single task before passing back to the standard AI.
 * Similar to the inaccessible "maneuvers" from the base game. */
interface TransientAI : AI {
	/** Store reference to previous [AI]. */
	val parent: AI

	/** Halt and yield to the [parent] AI. */
	fun yield()

	// Add default body
	override fun render() {}
}

/** Unwind [parent][TransientAI.parent] AIs and return first not implementing [TransientAI]. */
val CombatEntityAPI.permanentAI: AI
	get() {
		var result = ai as AI
		while (result is TransientAI)
			result = result.parent

		return result
	}

/** Get [EngineAI] from vanilla [AI]. */
val AI.motorAI: EngineAI?
	get() {
		return when (this) {
			is BasicShipAI -> engineAI
			is DroneAI -> engineAI
			is FighterAI -> engineAI
			else -> null
		}
	}

/** Force all [transient AIs][TransientAI] to [yield][TransientAI.yield], return permanent AI. */
fun CombatEntityAPI.resetParentAI(): AI {
	var result = ai as AI
	while (result is TransientAI) {
		result.yield()
		result = result.parent
	}

	return result
}
