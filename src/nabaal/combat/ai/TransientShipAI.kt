package nabaal.combat.ai

import com.fs.starfarer.api.combat.*
import com.fs.starfarer.combat.entities.Ship

/** Default implementations of [ShipAIPlugin] methods with little/no bearing on [transient AIs][TransientAI]. */
interface TransientShipAI : ShipAIPlugin, TransientAI {
	companion object Constants {
		val flags = ShipwideAIFlags()
		val cfg = ShipAIConfig()
	}

	var ship: Ship

	override fun getAIFlags(): ShipwideAIFlags {
		return flags
	}

	override fun getConfig(): ShipAIConfig {
		return cfg
	}

	override fun needsRefit(): Boolean {
		return false
	}

	override fun setDoNotFireDelay(p0: Float) {}
	override fun cancelCurrentManeuver() {}
	override fun forceCircumstanceEvaluation() {}

	override fun yield() {
		ship.shipAI = parent as ShipAIPlugin
	}
}
